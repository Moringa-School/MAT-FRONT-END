import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";
import { map, take } from "rxjs/operators";
import { IAuthResponse } from "src/app/interfaces/auth.interface";
import { GlobalAuthService } from "../../http/authentication/auth/newauth.service";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private authService: GlobalAuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.currentUserValue.pipe(take(1),map((authResponse:IAuthResponse)=>{
      console.log(authResponse);
      if (authResponse) {
        return true;
      }
      this.router.navigate(['/auth/login/'])
      return false
  }))

  }
}
