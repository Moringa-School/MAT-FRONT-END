import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CoreRoutingModule } from "./core-routing.module";

import { MaterialModule } from "../shared/material.module";

@NgModule({
  declarations: [],
  exports: [],
  imports: [MaterialModule, CommonModule, CoreRoutingModule],
})
export class CoreModule {}
