import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MaterialModule } from '../shared/material.module';

const routes: Routes = [];

@NgModule({
  imports: [MaterialModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
