import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FinalListService } from './final-list.service';

describe('FinalListService', () => {
  let service: FinalListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(FinalListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
