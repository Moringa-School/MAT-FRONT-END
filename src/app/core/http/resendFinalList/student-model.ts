import { IStatusEmail } from "src/app/interfaces/email.interface";

export const students: IStatusEmail[] = [
    {
        first_name: 'peter',
        last_name: 'muturi',
        email: 'peter@mail.com',
        class: 'Mc8',
        track: 'Fullstack',
        recommendation: 'Yes',
        status: true
    },
    {
        first_name: 'wesley',
        last_name: 'mutwiri',
        email: 'Wesley@me.com',
        class: 'Mc8',
        track: 'Fullstack',
        recommendation: 'Yes',
        status: false
    },
    {
        first_name: 'Elon',
        last_name: 'musk',
        email: 'tesla@musk.com',
        class: 'Mc8',
        track: 'Fullstack',
        recommendation: 'probation',
        status: false
    },
];
