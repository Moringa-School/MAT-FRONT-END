import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { students } from './student-model';

@Injectable({
  providedIn: 'root'
})
export class FinalListService {

  url = `${environment.BASE_URL}/resendStatusEmail/`;
  constructor(private http: HttpClient) { }

  getFinalList() {
    return of(students);
  }

  resendEmail(data: Object): Observable<any> {
    return this.http.post<any>(this.url, data);
  }
}
