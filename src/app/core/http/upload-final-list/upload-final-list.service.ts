import { Injectable, isDevMode } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";
import { Observable,  } from 'rxjs';


@Injectable({
    providedIn: "root",
})


export class StatusEMailService {
    public finalListURL: string
    public midModuleListURL

    getBaseUrlFromEnvironment() {
        return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
    }

    constructor(private httpClient: HttpClient) {
        const baseUrl = this.getBaseUrlFromEnvironment();
        this.finalListURL = `${baseUrl}students/send-final-status-emails/`;
        this.midModuleListURL = `${baseUrl}students/send-status-emails/`;
    }

    public uploadFinalList(data): Observable<any> {
        return this.httpClient.post(this.finalListURL, data,{
            reportProgress: true,
            observe: 'events'
          })
      }

    public uploadMidModuleList(data): Observable<any> {
        return this.httpClient.post(this.midModuleListURL, data, {
          reportProgress: true,
          observe: 'events'
        })
    }

  
    public errorMgmt(error: HttpErrorResponse)  {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
        // Get client-side error
        errorMessage = error.error.message;
      } else {
        // Get server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      return Observable.throw(error.status);
    }
      

}
