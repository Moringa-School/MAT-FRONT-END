import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";
import { ICohortResponse } from "src/app/interfaces/cohort.interface";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class GetCohortService {
  apiURL: string;

  constructor(private httpClient: HttpClient) {
    const baseUrl = this.getBaseUrlFromEnvironment();
    this.apiURL = `${baseUrl}cohorts/list/`;
  }

  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  getCohorts(): Observable<ICohortResponse> {
    return this.httpClient.get<ICohortResponse>(this.apiURL);
  }
}
