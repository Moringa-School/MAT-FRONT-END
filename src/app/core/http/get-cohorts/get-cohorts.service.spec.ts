import { GetCohortService } from './get-cohort.service';
import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { ICohortResponse } from '../../../interfaces/cohort.interface'

/* tslint:disable */
describe('List Students Service', () => {
  let cohortApiUrl: string;
  const mockCohorts: ICohortResponse = { cohorts:[] }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GetCohortService]
    })
    cohortApiUrl = `${environment.BASE_URL}cohorts/list/`
  })

  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify()
  }))

  it('should make a GET request to the cohorts endpoint', async(
    inject(
      [GetCohortService, HttpTestingController],
      (service: GetCohortService, httpMock: HttpTestingController) => {
        service.getCohorts().subscribe(res => {
            expect(res).toEqual(mockCohorts)
        })
        const mockReq = httpMock.expectOne(
          { method: 'GET', url: cohortApiUrl });
        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.method).toBe('GET')
        expect(mockReq.request.responseType).toEqual('json');
        mockReq.flush(mockCohorts);
        httpMock.verify();
      }
    )
  ))
});
