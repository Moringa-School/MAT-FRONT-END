import { Observable } from 'rxjs';
import { Injectable,isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";
@Injectable({
  providedIn: 'root'
})
export class InviteTmsService {

  private inviteTMURL: string;

  constructor( private httpClient: HttpClient) {
      const baseUrl = this.getBaseUrlFromEnvironment();
      this.inviteTMURL = `${baseUrl}invite-tms/`;
    }

    getBaseUrlFromEnvironment() {
      return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
    }

    public inviteTM(tmInviteCredentials):Observable<any>{
      return this.httpClient.post(this.inviteTMURL,tmInviteCredentials);
    }
}
