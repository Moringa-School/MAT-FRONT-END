import { Observable } from 'rxjs';
import { Injectable,isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";
@Injectable({
  providedIn: 'root'
})
export class TmProfileService {

  private tmProfileUrl: string;
  token = JSON.parse(localStorage.getItem("accessToken"));
  getIdFromToken(token) {
    try {
      const userInfo = JSON.parse(atob(token.split('.')[1]));
      if(userInfo){
        const { user_id} = userInfo; 
        return user_id
      }
    } catch (e) {
      return null;
    }
  }

  userId = this.getIdFromToken(this.token);
  headers = new HttpHeaders().set("Authorization", "Bearer " + this.token);
  httpOptions: { headers: HttpHeaders; };
  http: any;

  constructor( private httpClient: HttpClient) {
      const baseUrl = this.getBaseUrlFromEnvironment();
      this.tmProfileUrl = `${baseUrl}tm/3/`;
    }

    getBaseUrlFromEnvironment() {
      return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
    }

    public getTmDetails():Observable<any>{
      return this.httpClient.get( this.tmProfileUrl, {
        headers: this.headers,
      });
    }
}
