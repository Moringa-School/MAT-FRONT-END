import { TestBed, inject } from '@angular/core/testing';
import { InviteTmsService } from './invite-tms.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment'

describe('InviteTmsService', () => {
  let service: InviteTmsService;
  let httpMock: HttpTestingController;
  let baseUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InviteTmsService]
    });

    service = TestBed.get(InviteTmsService);
    httpMock = TestBed.get(HttpTestingController);
    baseUrl = `${environment.BASE_URL}invite-tms/`;
  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should be created', inject([InviteTmsService], (service: InviteTmsService) => {
    expect(service).toBeTruthy();
  }));
});
