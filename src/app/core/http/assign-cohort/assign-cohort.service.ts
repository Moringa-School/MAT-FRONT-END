import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class AssignTMToCohortService {
  apiURL: string;

  constructor(private httpClient: HttpClient) {
    const baseUrl = this.getBaseUrlFromEnvironment();
    this.apiURL = `${baseUrl}assign-cohort`;
  }

  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  assignTmToCohort = (tmAssignToCohort) => {
    return this.httpClient.patch(this.apiURL, tmAssignToCohort);
  };
}

