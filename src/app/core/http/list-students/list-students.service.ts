import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IStudent } from "../../../interfaces/data.interface";
import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class ListStudentsService {
  apiURL: string;

  constructor(private http: HttpClient) {
    const BASE_URL = this.getBaseUrlFromEnvironment();
    this.apiURL = `${BASE_URL}students`;
  }

  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  getStudents = (pageNumber) => {
    return this.http.get(`${this.apiURL}?page=${pageNumber}`);
  };

  getStudentsInCohort = (cohortId,pageNumber =1 ) => {
    return this.http.get(`${this.apiURL}?cohort__id=${cohortId}&page=${pageNumber}`, {
    });
  }
}
