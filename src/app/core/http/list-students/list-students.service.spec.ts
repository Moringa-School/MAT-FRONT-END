import { ListStudentsService } from './list-students.service';
import { TestBed, inject, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpEvent, HttpEventType, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
/* tslint:disable */
describe('List Students Service', () => {
  let apiUrl : string;
  let studentCohortApiUrl: string;
  const mockStudents = {
    "students": {
      "count": 10,
      "next": 'string',
      "previous": 'string',
      "results": [
        {
          "first_name": 'string',
          "last_name": 'string',
          "email": 'string',
          "username": 'string',
          "created_at": 'string',
          "updated_at": 'string'
        }
      ]
    }
  }
  const mockCohorts = {
    "cohorts": [
      {
          "name": "string",
          "id": 9,
          "created_at": "string",
          "updated_at": "string",
          "start_date": "string",
          "end_date": "2string"
      },
    ]
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ListStudentsService]
    })
    apiUrl = `${environment.BASE_URL}students?page=1`
    studentCohortApiUrl = `${environment.BASE_URL}students?cohort__id=1&page=1`
  })

  afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
    backend.verify()
  }))

  it('should make a GET request to the students endpoint', async(
    inject(
      [ListStudentsService, HttpTestingController],
      (service: ListStudentsService, httpMock: HttpTestingController) => {
        service.getStudents(1).subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockStudents);
          }
        })

        const mockReq = httpMock.expectOne(
          { method: 'GET', url: apiUrl });

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.method).toBe('GET')
        expect(mockReq.request.responseType).toEqual('json');

        mockReq.flush(mockStudents);

        httpMock.verify();
      }
    )
  )
  )
  it('should make a GET request to the cohorts endpoint', async(
    inject(
      [ListStudentsService, HttpTestingController],
      (service: ListStudentsService, httpMock: HttpTestingController) => {
        service.getStudentsInCohort(1,1).subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockCohorts);
          }
        })

        const mockReq = httpMock.expectOne(
          { method: 'GET', url: studentCohortApiUrl });

        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.method).toBe('GET')
        expect(mockReq.request.responseType).toEqual('json');

        mockReq.flush(mockStudents);

        httpMock.verify();
      }
    )
  )
  )

});
