
import { MatSnackBarModule } from '@angular/material';
// This module here registers all services created in the services folder

import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorIntercept } from './error.interceptor';

import { ListStudentsService } from './list-students/list-students.service';
import { SnackBarService } from './snackbar/mat-snack-bar.service';

@NgModule({
    imports: [HttpClientModule, MatSnackBarModule],
    providers: [
        ListStudentsService,
        { provide: HTTP_INTERCEPTORS, useClass: ErrorIntercept, multi: true },
        SnackBarService
    ]
})

export class ServicesModule { }