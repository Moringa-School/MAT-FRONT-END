import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class CreateCohortService {
  public apiURL: string


  constructor(private httpClient: HttpClient) {
    const baseUrl = this.getBaseUrlFromEnvironment();
    this.apiURL = `${baseUrl}cohorts/`;
  }

  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  public createCohort(cohortDetails): Observable<any> {
    return this.httpClient.post(this.apiURL, cohortDetails);
  }

}
