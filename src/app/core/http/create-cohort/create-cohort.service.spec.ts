import { CreateCohortService } from './create-cohort.service';
import { TestBed, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
/* tslint:disable */
describe('Create cohort Service', () => {

  let listStudentsService: CreateCohortService;
  let httpMock: HttpTestingController
  let pageNumber: number = 1;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CreateCohortService]
    });

    listStudentsService = TestBed.get(CreateCohortService);
    httpMock = TestBed.get(HttpTestingController);
    localStorage.setItem('token', 'my-long-token');

  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should call endpoint successfully with the right route', fakeAsync(() => {
    listStudentsService.createCohort({ name: "MPFT28" }).subscribe(res => {
      expect(res).toEqual([])
    })

    const mockReq = httpMock.expectOne(req => req.method === 'POST' && req.url === listStudentsService.apiURL);

    expect(mockReq.cancelled).toBeFalsy();
    expect(mockReq.request.method).toBe('POST')
    expect(mockReq.request.responseType).toEqual('json');

    httpMock.verify();
  }));
});
