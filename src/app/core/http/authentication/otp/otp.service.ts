import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment as devEnvironment } from "src/environments/environment";
import { environment as prodEnvironment } from "src/environments/environment.prod";
import { Observable } from "rxjs";
import {
  generateOTP,
  verifyOTP,
  verifyOTPResponse,
} from "src/app/interfaces/otp.interface";

@Injectable({
  providedIn: "root",
})
export class OtpServiceService {
  protected apiURL: string;

  constructor(private http: HttpClient) {
    const BASE_URL = this.getBaseURLFromEnvrionment();
    this.apiURL = `https://matapi.work/mat-otp/api`;
  }

  private getBaseURLFromEnvrionment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  generateOTP(): Observable<generateOTP> {
    return this.http.post<any>(`${this.apiURL}/generate-otp`, {});
  }

  verifyOTP(args: verifyOTP): Observable<verifyOTPResponse> {
    return this.http.post<verifyOTPResponse>(`${this.apiURL}/verify-otp`, args);
  }
}
