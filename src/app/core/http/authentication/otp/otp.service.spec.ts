import { TestBed } from "@angular/core/testing";

import { OtpServiceService } from "./otp.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("OtpService", () => {
  let service: OtpServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(OtpServiceService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
