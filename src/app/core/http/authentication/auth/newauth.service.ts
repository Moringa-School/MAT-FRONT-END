import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { IAuthResponse } from "src/app/interfaces/auth.interface";
import { environment } from "src/environments/environment";

@Injectable({ providedIn: "root" })
export class GlobalAuthService {
    private userSubject: BehaviorSubject<IAuthResponse>;
    public currentUser: Observable<IAuthResponse>;
    private loginStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    // private router: Router;


    constructor(private http: HttpClient, private router: Router) {
        this.userSubject = new BehaviorSubject<IAuthResponse>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.userSubject.asObservable();
    }

    public get currentUserValue(): Observable<IAuthResponse> {
        return this.currentUser;
    }


    authenticateUserWithServer(loginCredentials): Observable<IAuthResponse> {
        return this.http.post<IAuthResponse>(`${environment.BASE_URL}login`, loginCredentials)
            .pipe(map(user => {
                localStorage.setItem('currentUser', JSON.stringify(user));

                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.userSubject.next(null);
        this.router.navigate(['/auth/login'])
    }
}