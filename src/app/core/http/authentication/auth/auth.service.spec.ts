import { IAuthResponse, ISocialAuthBody } from './../../../../interfaces/auth.interface';
import { GlobalAuthService } from './newauth.service';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IAuthBody } from 'src/app/interfaces/auth.interface';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';
import { AuthService } from 'angularx-social-login';
/* tslint:disable */
describe('Auth Service', () => {
  let httpMock: HttpTestingController;
  let loginURL: string;
  let socialLoginURL: string;
  const tokenResponse: IAuthResponse = { refresh: "string", access: "string" }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),],
    });
    socialLoginURL = `${environment.BASE_URL}login/oauth`;
    loginURL = `${environment.BASE_URL}login`;
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  })
  it('should create service', inject([GlobalAuthService], (authService: GlobalAuthService) => {
    expect(authService).toBeTruthy();
  }));
  it('should call login user function with correct parameters and to correct route',
    inject([GlobalAuthService], (authService: GlobalAuthService) => {
      const loginCredentials: IAuthBody = { email: "test@gmail.com", password: "password" };
      spyOn(authService, "authenticateUserWithServer").and.callThrough();
      authService.authenticateUserWithServer(loginCredentials).subscribe(res => {
        expect(res).toEqual(tokenResponse)
      });
      const httpRequest = httpMock.expectOne(req => req.method === 'POST' && req.url === loginURL);
      expect(httpRequest.request.method).toEqual('POST');
      httpRequest.flush(tokenResponse)
    }));

  xit('should call social login user function with correct parameters and to correct route',
    inject([GlobalAuthService], (authService: GlobalAuthService) => {
      let token = 'sdfdghgfdsadsFdgfh'
      const loginCredentials: ISocialAuthBody = { token };
      spyOn(authService, "authenticateUserWithServer").and.callThrough();
      authService.authenticateUserWithServer(loginCredentials).subscribe(res => {
        expect(res).toEqual(tokenResponse)
      });
      const httpRequest = httpMock.expectOne(req => req.method === 'POST' && req.url === socialLoginURL);
      expect(httpRequest.request.method).toEqual('POST');
      httpRequest.flush(tokenResponse)
    }));

});
