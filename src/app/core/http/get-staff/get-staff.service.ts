import { HttpClient } from '@angular/common/http';
import { isDevMode, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IStaffResponse } from 'src/app/interfaces/staff.interface';


import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})

export class GetStaffService {
  apiURL: string;

  constructor(private httpClient: HttpClient) {
    const baseUrl = this.getBaseUrlFromEnvironment();
    this.apiURL = `${baseUrl}staff/`;
  }

  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  getAllStaff(): Observable<IStaffResponse> {
    return this.httpClient.get<IStaffResponse>(this.apiURL);
  }
}