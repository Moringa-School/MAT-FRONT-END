import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EmailService } from './email.service';

describe('EmailService', () => {
    const data = [{ firstName: 'Henry', lastName: 'Onyango', email: 'henry.onyango@moringaschool.com', macAddress: '1234566' }];
    let service: EmailService; // rename
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule ],
            providers: [ EmailService ]
       });
       service = TestBed.get(EmailService);
       httpMock = TestBed.get(HttpTestingController);
    });
});