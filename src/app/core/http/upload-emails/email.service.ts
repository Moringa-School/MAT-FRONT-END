import { Injectable, isDevMode } from "@angular/core";
import { IEmailResponse } from "../../../interfaces/email.interface";
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { environment as devEnvironment } from "../../../../environments/environment";
import { environment as prodEnvironment } from "../../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class EmailService {
  token: String;
  baseUrl: String;
  proxyurl = "https://cors-anywhere.herokuapp.com/";


  constructor(private httpClient: HttpClient) {
    this.baseUrl = this.getBaseUrlFromEnvironment();

  }

  // TODO: move this to util class
  getBaseUrlFromEnvironment() {
    return isDevMode ? devEnvironment.BASE_URL : prodEnvironment.BASE_URL;
  }

  // uploading data to serve
  bulkUploadEmail = (studentEmails: string[]): Observable<any> => {
    const sendEmailURL = `${this.baseUrl}staff/send-mail`;
    const bulkEmailPayload = {
      subject: "Welcome to Moringa",
      message: "Click on the link below to activate your account",
      recipients: studentEmails,
    };
    return this.httpClient.post<any>(sendEmailURL, bulkEmailPayload);
  }

  sendUploadedFileToServer(file): Observable<IEmailResponse> {
    const emailUploadURL = `${this.baseUrl}students/upload`;

    return this.httpClient.post<IEmailResponse>(emailUploadURL, file, {
      headers: {
        "Content-Disposition": "attachment; filename=file",
      },
    });
  }
}
