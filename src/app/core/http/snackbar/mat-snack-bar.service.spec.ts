import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SnackBarService } from './mat-snack-bar.service';


describe('SnackBarService', () => {

  let httpTestingController: HttpTestingController;
  let snackBarServiceTest: SnackBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, HttpClientTestingModule, BrowserAnimationsModule],
      providers: [SnackBarService]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    snackBarServiceTest = TestBed.get(SnackBarService);
  });

  it('should be created', () => {
    const service: SnackBarService = TestBed.get(SnackBarService);
    expect(service).toBeTruthy();
  });

  it('should call showerrorSnackBar', fakeAsync(() => {
    const spyShowErrorSnackBar = spyOn(snackBarServiceTest, 'showErrorSnackBar').and.callThrough();
    snackBarServiceTest.showErrorSnackBar('message sent');
    tick(5000);
    expect(spyShowErrorSnackBar).toHaveBeenCalledWith('message sent');

  }));

  it('should call showSucessSnackBar', fakeAsync(() => {
    const spShowSuccessSnackBar = spyOn(snackBarServiceTest, 'showSuccessSnackBar').and.callThrough();
    snackBarServiceTest.showSuccessSnackBar('message sent');
    tick(5000);
    expect(spShowSuccessSnackBar).toHaveBeenCalledWith('message sent');

  }));
});
