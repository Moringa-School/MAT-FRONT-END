import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map, take } from "rxjs/operators";
import { IAuthResponse } from "src/app/interfaces/auth.interface";
import { environment } from "src/environments/environment";
import { GlobalAuthService } from "../../http/authentication/auth/newauth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    token: string;
    constructor(private authService:GlobalAuthService){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const currentUser = this.authService.currentUserValue;
        const isLoggedIn = currentUser;
        const isApiUrl = req.url.startsWith(environment.BASE_URL);
        
        const savedToken = currentUser.subscribe( res =>{
             this.token = res.access;
        });

        if (isLoggedIn && isApiUrl) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.token}`
                }
            });
        }

        return next.handle(req);
    }
}