import { ErrorIntercept } from './error.interceptor';

import { throwError } from 'rxjs';

// TODO: Move assigning spyObjects to beforeEach
describe('ErrorInterceptor', () => {
  let errorInterceptor;

  beforeEach(() => {
    errorInterceptor = new ErrorIntercept();
  });

  describe('Interceptor =>', () => {
    it('should create the http interceptor', () => {
      expect(errorInterceptor).toBeTruthy();
    });
  });

  describe('Actions =>', () => {
    let httpRequestSpy;
    let httpHandlerSpy;

    it('should return a server side error', () => {
      const error = { status: 404, message: 'test-error' };

      httpRequestSpy = jasmine.createSpyObj('HttpRequest', ['doesNotMatter']);
      httpHandlerSpy = jasmine.createSpyObj('HttpHandler', ['handle']);
      httpHandlerSpy.handle.and.returnValue(throwError(error));

      errorInterceptor.intercept(httpRequestSpy, httpHandlerSpy).subscribe(
        // remove result from here. Not needed
        result => console.log('request successful', result),
        err => {
          expect(err).toEqual('Error occurred: test-error');
        });
    });

    it('should return a client side error', () => {
      const error = { error: new ErrorEvent('Bad request', { message: 'Bad request' }) };

      httpRequestSpy = jasmine.createSpyObj('HttpRequest', ['doesNotMatter']);
      httpHandlerSpy = jasmine.createSpyObj('HttpHandler', ['handle']);
      httpHandlerSpy.handle.and.returnValue(throwError(error));

      errorInterceptor.intercept(httpRequestSpy, httpHandlerSpy).subscribe(
        (result: any) => console.log('good', result),
        (err: any) => {
          expect(err).toEqual('Error occurred: Bad request');
        });
    });
  });
});
