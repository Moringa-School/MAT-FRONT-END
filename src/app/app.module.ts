import { environment } from './../environments/environment';
import { ErrorHandler, Injectable, NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import * as Sentry from "@sentry/browser";
import {
  AuthServiceConfig,
  GoogleLoginProvider,
  SocialLoginModule,
} from "angularx-social-login";
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from "ngx-toastr";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AuthGuardService } from "./core/guards/auth/auth.guard";

import { BrowserModule } from "@angular/platform-browser";
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { TopNavComponent } from "./shared/components/top-nav/top-nav.component";
import { MaterialModule } from "./shared/material.module";
import { AuthInterceptor } from './core/interceptors/auth/auth.interceptor';
import { MatSelectModule } from '@angular/material';


  const config = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(environment.GOOGLELOGINPROVIDER)
    },
  ]);
  
export function provideConfig() {
  return config;
}

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error: any) {
    const eventId = Sentry.captureException(error.originalError || error);
  }
}

@NgModule({
  declarations: [AppComponent, TopNavComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    MaterialModule,
    SharedModule,
    CoreModule,
    SocialLoginModule,
    FlexLayoutModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatSelectModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig,
      
    },
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true},
    AuthGuardService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
