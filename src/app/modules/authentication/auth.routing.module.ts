import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { LoginOtpComponent } from "./login-otp/login-otp.component";
import { MaterialModule } from "src/app/shared/material.module";
import { GenerateOtpComponent } from "./generate-otp/generate-otp.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "verify-otp", component: LoginOtpComponent },
  { path: "generate-otp", component: GenerateOtpComponent },
];

@NgModule({
  imports: [MaterialModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
