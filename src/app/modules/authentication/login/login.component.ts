import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider } from 'angularx-social-login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IAuthBody, ISocialAuthBody } from 'src/app/interfaces/auth.interface';
import { Router } from '@angular/router';
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { GlobalAuthService } from 'src/app/core/http/authentication/auth/newauth.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

    constructor(
        private formBuilder: FormBuilder,
        private authService: GlobalAuthService,
        private router: Router,
        private socialAuthService: AuthService,
        private snackBar: SnackBarService,
    ) { }

    form: FormGroup;
    submitted = false;
    isLoading = false;
    successMessage = 'Successfully logged In!';
    errorMessage = 'Wrong credentials.Unsuccessful Login!';


    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern('[^ @]*@[^ @]*')]],
            password: ['', Validators.required]
        });

    }
    loginUser = () => {
        if (this.form.valid) {
            this.isLoading = true;
            const loginCredentials: IAuthBody = {
                email: this.form.controls.email.value,
                password: this.form.controls.password.value
            };
            this.authService.authenticateUserWithServer(loginCredentials).subscribe(
                res => {
                    this.isLoading = false;
                    this.snackBar.showSuccessSnackBar(this.successMessage);
                    this.router.navigate(['students']);
                }, error => {
                    this.isLoading = false;
                    this.snackBar.showErrorSnackBar(this.errorMessage);
                });
        }
    }


    signInWithGoogle = () => {
        this.isLoading = true;
        this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
            .then(data => {
                const { idToken } = data;
                const token = idToken;
                const socialLoginCredentials: ISocialAuthBody = { token };

            });

    }
}
