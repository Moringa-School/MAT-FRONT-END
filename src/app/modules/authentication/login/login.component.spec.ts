import { IAuthResponse, IAuthBody, ISocialAuthBody } from 'src/app/interfaces/auth.interface';
import { async, ComponentFixture, TestBed, tick, inject, fakeAsync } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
// import { SocialUser } from "angularx-social-login";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GlobalAuthService } from '../../../core/http/authentication/auth/newauth.service';
import { SocialLoginModule, AuthService, AuthServiceConfig, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { provideConfig } from 'src/app/app.module';
import { DebugElement } from '@angular/core';
import { MatSnackBarModule } from '@angular/material';
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { of } from 'rxjs/internal/observable/of';
/* tslint:disable */
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let snackBarServiceTest: SnackBarService;
  let mockResponse: SocialUser = {
    authorizationCode: "fghjklkjh",
    authToken: "ya29.a0AfH6SMBgsIO0G7nwE",
    email: "sly.mbu@moringaschool.com",
    firstName: "sly ",
    id: "1183932380",
    idToken: "eiOiJKVdxdOiS1FvWmJtcmp0aWxNNlpuRUNfUGN",
    lastName: "user",
    name: "test",
    photoUrl: "https://lh3.go67IE5AGtFXlnN17=s96-c",
    provider: "GOOGLE"
  }
  let tokenResponse: IAuthResponse = {
    access: 'dfghjklkjhgfghjkjhghjkjhg',
    refresh: 'hhghhgfzxcvjgfdsdfghhgfhh'
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatInputModule,
        RouterTestingModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
      ],
      declarations: [
        LoginComponent
      ],
      providers: [SocialLoginModule, AuthService,
        {
          provide: AuthServiceConfig,
          useFactory: provideConfig
        }]
    });
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    component.isLoading = true;
    snackBarServiceTest = TestBed.get(SnackBarService);
    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
  }));

  it('should be created', inject([GlobalAuthService], (service: GlobalAuthService) => {
    expect(service).toBeTruthy();
  }));

  it('should be created', inject([SnackBarService], (service: SnackBarService) => {
    expect(service).toBeTruthy();
  }));

  // check that router-outlet is present
  it("should have a form with username input, password input and button to login", () => {
    const element = fixture.debugElement;
    expect(element.queryAll(By.css(".username")).length).toBe(1);
    expect(element.queryAll(By.css(".password")).length).toBe(1);
    //se the is loading attribute to be true to have both buttons
    component.isLoading = false;
    //run fixture.detectchanges to run ngOnit once more
    fixture.detectChanges();
    expect(
      element.queryAll(By.css("button"))[0].nativeElement.textContent
    ).toBe("Login");
    //check if the second button is Login with Google
    expect(
      element.queryAll(By.css("button"))[1].nativeElement.textContent
    ).toBe(" Login with Google ");
  });

  // // confirm that form is valid
  it('should return a model invalid when the form is empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  // validate email required
  it('should validate that email field is required', () => {
    let email = component.form.controls['email'];
    expect(email.valid).toBeFalsy();
    expect(email.errors.required).toBeTruthy();
  });

  // validate that password is required
  it('should validate that password is a required field', () => {
    let password = component.form.controls['password'];
    expect(password.valid).toBeFalsy();
    expect(password.errors.required).toBeTruthy();
  });

  // validate email is the right format
  it('should validate email format', () => {
    let email = component.form.controls['email'];
    email.setValue('test@gmail.com');
    let errors = email.errors || {};
    expect(email.valid).toBeTruthy();
    expect(errors.required).toBeFalsy();
    expect(errors.pattern).toBeFalsy();
  });
  // validate that login button calls function
  it('should call the login user function', fakeAsync(() => {
    component.isLoading = false;
    fixture.detectChanges();
    let loginButton = fixture.debugElement.query(By.css('.login-btn'));
    const spy = spyOn(component, 'loginUser');
    loginButton.nativeElement.click()
    tick();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }))

  //check for response on calling login user method
  it('should login user', () => {
    let loginElement: DebugElement;
    const debugElement = fixture.debugElement;
    let loginAuthService = debugElement.injector.get(GlobalAuthService);
    let loginSpy = spyOn(loginAuthService, 'authenticateUserWithServer').and.returnValue(of(tokenResponse));
    loginElement = fixture.debugElement.query(By.css('form'));
    component.form.controls['email'].setValue('test@gmail.com');
    component.form.controls['password'].setValue('password');
    loginElement.triggerEventHandler('ngSubmit', null);
    fixture.whenStable().then(() => {
      expect(loginSpy).toHaveBeenCalled();
      expect(mockResponse).not.toBeNull();
    });

  });

  //test user can login with google
  it('should login user with google', () => {
    const debugElement = fixture.debugElement;
    let authService = debugElement.injector.get(AuthService);
    let spySignIn = spyOn(authService, 'signIn').and.returnValue(Promise.resolve(mockResponse))
    component.signInWithGoogle()
    expect(spySignIn).toHaveBeenCalled()
  });

});

