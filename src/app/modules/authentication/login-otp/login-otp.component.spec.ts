import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LoginOtpComponent } from "./login-otp.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
import { OtpServiceService } from "src/app/core/http/authentication/otp/otp.service";

describe("LoginOtpComponent", () => {
  let component: LoginOtpComponent;
  let fixture: ComponentFixture<LoginOtpComponent>;

  let mockOtpService = jasmine.createSpyObj(["verifyOTP"]);
  let mockSpinnerService = jasmine.createSpyObj(["show", "hide"]);
  let mocktoastrService = jasmine.createSpyObj(["success", "error"]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginOtpComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [
        { provide: OtpServiceService, useValue: mockOtpService },
        { provide: ToastrService, useValue: mocktoastrService },
        { provide: NgxSpinnerService, useValue: mockSpinnerService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should call otp.verifyOTP when verify OTP is called", () => {
    spyOn(component, "verifyOTP");
    mockOtpService.verifyOTP.and.returnValue(of({ success: true }));

    fixture.detectChanges();

    fixture.debugElement
      .query(By.css("button"))
      .triggerEventHandler("click", {});

    fixture.detectChanges();
    expect(component.verifyOTP).toHaveBeenCalled();
  });
});
