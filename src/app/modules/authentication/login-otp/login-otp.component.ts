import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { OtpServiceService } from "src/app/core/http/authentication/otp/otp.service";

@Component({
  selector: "app-login-otp",
  templateUrl: "./login-otp.component.html",
  styleUrls: ["./login-otp.component.scss"],
})
export class LoginOtpComponent implements OnInit {
  otpForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private otpService: OtpServiceService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.otpForm = this.fb.group({
      otp: [, [Validators.required, Validators.min(6)]],
    });
  }

  verifyOTP() {
    this.spinner.show();
    this.otpService.verifyOTP(this.otpForm.value).subscribe(
      (data) => {
        this.toastr.success(data.message, "success", {
          progressBar: true,
          timeOut: 3000,
          closeButton: true,
        });
        this.spinner.hide();
      },
      (error) => {
        this.toastr.error(
          "We could not verify this OTP, kindly check and try again",
          "Error",
          {
            progressBar: true,
            timeOut: 3000,
            closeButton: true,
          }
        );
        this.spinner.hide();
      }
    );
  }
}
