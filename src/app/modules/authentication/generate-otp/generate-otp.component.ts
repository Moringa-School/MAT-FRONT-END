import { Component, OnInit } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { OtpServiceService } from "src/app/core/http/authentication/otp/otp.service";

@Component({
  selector: "app-generate-otp",
  templateUrl: "./generate-otp.component.html",
  styleUrls: ["./generate-otp.component.scss"],
})
export class GenerateOtpComponent implements OnInit {
  otpvalue = new BehaviorSubject<number>(null);
  constructor(
    private otpService: OtpServiceService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {}

  generateOtp() {
    this.spinner.show();
    this.otpService.generateOTP().subscribe((data) => {
      this.toastr.success(data.message);
      this.otpvalue.next(data.otp);
      this.spinner.hide();
    });
  }
}
