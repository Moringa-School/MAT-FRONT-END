import { GenerateOtpComponent } from "./generate-otp.component";
import { ComponentFixture, TestBed, async } from "@angular/core/testing";
import { FormBuilder } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
import { OtpServiceService } from "src/app/core/http/authentication/otp/otp.service";

describe("LoginOtpGenerateComponent", () => {
  let component: GenerateOtpComponent;
  let fixture: ComponentFixture<GenerateOtpComponent>;

  let mockFormBuilder = {
    fb: {
      group: {},
    },
  };

  let mockOtpService = jasmine.createSpyObj(["generateOTP"]);
  let mockSpinnerService = jasmine.createSpyObj(["show", "hide"]);
  let mocktoastrService = jasmine.createSpyObj(["success", "error"]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateOtpComponent],
      providers: [
        { provide: FormBuilder, useValue: mockFormBuilder },
        { provide: OtpServiceService, useValue: mockOtpService },
        { provide: ToastrService, useValue: mocktoastrService },
        { provide: NgxSpinnerService, useValue: mockSpinnerService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should call otpService.generateOTP when generate OTP is clicked", () => {
    spyOn(component, "generateOtp");
    mockOtpService.generateOTP.and.returnValue(of({ otp: 205205 }));
    //run ngOnit
    fixture.detectChanges();

    fixture.debugElement
      .query(By.css("button"))
      .triggerEventHandler("click", {});
    fixture.detectChanges();
    expect(component.generateOtp).toHaveBeenCalled();
  });
});
