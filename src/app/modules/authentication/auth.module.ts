import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LoginOtpComponent } from "./login-otp/login-otp.component";
import { GenerateOtpComponent } from "./generate-otp/generate-otp.component";
import { LoginComponent } from "./login/login.component";
import { MaterialModule } from "src/app/shared/material.module";
import { AuthRoutingModule } from "./auth.routing.module";

@NgModule({
  declarations: [LoginOtpComponent, GenerateOtpComponent, LoginComponent],
  imports: [MaterialModule, CommonModule, AuthRoutingModule],
})
export class AuthModule {}
