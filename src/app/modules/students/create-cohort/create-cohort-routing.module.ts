import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { CreateCohortComponent } from "./create-cohort.component";
import { AuthGuardService } from "src/app/core/guards/auth/auth.guard";

const listStudentsRoutes = [
  {
    path: "create-cohort",
    canActivate: [AuthGuardService],
    component: CreateCohortComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(listStudentsRoutes)],
  exports: [RouterModule],
})
export class CreateCohortRoutingModule {}
