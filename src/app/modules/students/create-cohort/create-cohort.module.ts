import { NgModule } from '@angular/core';
import  { CommonModule } from '@angular/common';

import { HttpClientModule } from  '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material';
import { CreateCohortComponent } from './create-cohort.component';
import { CreateCohortRoutingModule }  from './create-cohort-routing.module';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


@NgModule({
    imports: [
        CreateCohortRoutingModule,
        CommonModule,
        HttpClientModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatIconModule,
        MatSnackBarModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        CreateCohortComponent,
    ],
    exports: [ CreateCohortComponent ]
})

export class CreateCohortModule {}