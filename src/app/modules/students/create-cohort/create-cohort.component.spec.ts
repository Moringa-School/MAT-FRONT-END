import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule, MatDialogModule, MatDialogRef } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CreateCohortComponent } from './create-cohort.component';

describe('CreateCohortComponent', () => {
  let component: CreateCohortComponent;
  let fixture: ComponentFixture<CreateCohortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatInputModule,
        MatDialogModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule
      ],
      declarations: [CreateCohortComponent]
    })
      .compileComponents();

  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreateCohortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form with username input, password input and button to login', () => {
    const element = fixture.nativeElement;

    expect(element.querySelector('.form-input')).toBeTruthy();
    expect(element.querySelector('.create-cohort-button')).toBeTruthy();
  });

  it('should return a model invalid when the form is empty', () => {
    expect(component.createCohortForm.valid).toBeFalsy();
  });

  it('should validate that cohort name field is required', () => {
    let cohortName = component.createCohortForm.controls['cohort_name'];
    expect(cohortName.valid).toBeFalsy();
    expect(cohortName.errors.required).toBeTruthy();
  });

  it('should call the createCohort function', fakeAsync(() => {
    fixture.detectChanges();
    let createCohortButton = fixture.debugElement.query(By.css('.create-cohort-button'));
    createCohortButton.nativeElement.click()
    component.createCohort()
    tick()
    fixture.detectChanges();
    expect(component.submitted).toBeTrue();
  }))


});
