import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import * as moment from "moment";
import { CreateCohortService } from "src/app/core/http/create-cohort/create-cohort.service";
import { SnackBarService } from "../../../core/http/snackbar/mat-snack-bar.service";
import { YearMonthDate } from "../../../constants";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: "app-create-cohort",
  templateUrl: "./create-cohort.component.html",
  styleUrls: ["./create-cohort.component.scss"],
})
export class CreateCohortComponent implements OnInit {
  createCohortForm: FormGroup;
  submitted: boolean = false;
  cohort: Object = {};
  error: boolean = false;
  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private createCohortService: CreateCohortService,
    public snackBar: SnackBarService,
    public dialog: MatDialog
  ) {}
  successMessage = "Cohort created successfully!";

  ngOnInit() {
    this.createCohortForm = this.formBuilder.group({
      cohort_name: ["", [Validators.required]],
      start_date: ["", [Validators.required]],
      end_date: ["", [Validators.required]],
    });
  }

  createCohort = () => {
    this.submitted = true;
    this.isLoading = true;
    const { cohort_name, start_date, end_date } = this.createCohortForm.value;
    this.cohort = this.createCohortService
      .createCohort({
        cohort_name,
        start_date: moment(start_date).format(YearMonthDate),
        end_date: moment(end_date).format(YearMonthDate),
      })
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.error = false;
          this.snackBar.showSuccessSnackBar(this.successMessage);
          this.dialog.closeAll();
        },
        (error) => {
          this.snackBar.showErrorSnackBar(
            "There as an error, creating, ensure cohort name starts with MC"
          );
          this.isLoading = false;
          this.error = true;
        }
      );
  };

  pastDateFilter = (d: Date): boolean => {
    const day = moment(d);
    // Prevent Saturday and Sunday from being selected.
    return day.diff(new Date()) > 0;
  };
}
