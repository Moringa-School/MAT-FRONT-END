import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StudentStatusEmailResendComponent } from './student-status-email-resend.component';
import { MatFormFieldModule, MatInputModule, MatFormFieldControl } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

describe('StudentStatusEmailResendComponent', () => {
  let component: StudentStatusEmailResendComponent;
  let fixture: ComponentFixture<StudentStatusEmailResendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatFormFieldModule, MatInputModule, ReactiveFormsModule, FormsModule],
      declarations: [StudentStatusEmailResendComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentStatusEmailResendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
