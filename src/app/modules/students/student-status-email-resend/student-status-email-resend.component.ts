import { Component, OnInit, SimpleChange } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

import { FinalListService } from 'src/app/core/http/resendFinalList/final-list.service';
import { IStatusEmail } from 'src/app/interfaces/email.interface';

@Component({
  selector: 'app-student-status-email-resend',
  templateUrl: './student-status-email-resend.component.html',
  styleUrls: ['./student-status-email-resend.component.scss']
})
export class StudentStatusEmailResendComponent implements OnInit {

  columnsToDisplay: string[] = ['fullNames', 'studentEmail', 'finalRecommendation', 'statusEmail'];
  emailType: string;
  students: IStatusEmail[] = [];
  dataSource;

  constructor(private finalListService: FinalListService) { }

  ngOnInit(): void {
    this.getFinalList();
    this.dataSource = new MatTableDataSource(this.students);
  }

  getFinalList(): void {
    this.finalListService.getFinalList().subscribe(students => this.students = students);
  }

  search(event: Event) {
    const searchTerm = (event.target as HTMLInputElement).value;
    this.dataSource.filter = searchTerm.trim().toLowerCase();
  }

  statusChange(status: String) {
    this.dataSource.filter = status.trim().toLowerCase();
  }

  selectEmailOption(value: string) {
    if (value) {
      this.emailType = value;
      console.log(this.emailType);
    }
  }

  sendMail() {
    if (this.emailType) {
      let data = {}, studentEmails = [];
      this.dataSource.filteredData.forEach(element => {
        studentEmails.push({
          email: element.email,
          recommendation: element.recommendation,
        });
      });
      data = {
        emailType: this.emailType,
        students : studentEmails
      };
      this.finalListService.resendEmail(data).subscribe();
    } else {
      console.log('select option');
    }
  }
}
