import { Component, OnInit, ViewChild } from '@angular/core';
import { IEmail } from 'src/app/interfaces/email.interface';
import { EmailService } from '../../../core/http/upload-emails/email.service';
import { MatTable } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { EmailDataSource } from './email-data-source';
import { GetCohortService } from '../../../core/http/get-cohorts/get-cohort.service';
import { ICohort } from 'src/app/interfaces/cohort.interface';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { CheckEmailFormatService } from 'src/app/shared/services/check-email-format/check-email-format.service';

@Component({
    selector: 'app-upload-emails',
    templateUrl: './upload-emails.component.html',
    styleUrls: ['./upload-emails.component.scss']
})


export class UploadEmailsComponent implements OnInit {
    isLoaded = false;
    length = 0;
    pageSize = 10;
    pageSizeOptions: number[] = [10, 20, 50];
    pageEvent: PageEvent;
    dataSource: EmailDataSource;
    uploadedFile: File = null;
    studentEmails: string[];
    failedToCreateEmails: string[];
    uploadCompleted = false;
    currentCohorts: ICohort[] = [];
    classSelector = new FormControl();
    filteredClasses: Observable<ICohort[]>;
    noEmails = false;
    failedMaildataSource: string[];
    csvRecords: any[] = [];

    // TODO: Move the displayedColoumns to the html view
    displayedColumns: string[] = ['Username', 'email', 'firstname', 'secondname'];
    displayFailedMailColoumn: string[] = ['Email'];

    @ViewChild('dataUpload', { static: false }) dataUpload: any;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    @ViewChild(MatTable, { static: false }) table: MatTable<any>;

    constructor(
        private emailService: EmailService,
        private snackBar: SnackBarService,
        private getCohortService: GetCohortService,
        private ngxCsvParser: NgxCsvParser,
        private checkEmailFormat: CheckEmailFormatService) {
    }

    ngOnInit() {
        this.getCohortsAndAssignToDropdown();
        this.filteredClasses = this.classSelector.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
    }

    uploadCSVFile($event: any): void {
        const files = $event.srcElement.files;
        this.ngxCsvParser.parse(files[0], { delimiter: ',' }).pipe().subscribe((res: Array<any>) => {
            this.csvRecords.push(this.checkEmailFormat.validateEmail(res));
            this.callServiceToExtractDataAndUpdateEmails([...this.csvRecords[0].valid, ...this.csvRecords[0].invalid]);
        }, (error: NgxCSVParserError) => {
            console.log('Error', error);
        });
    }


    sendFileToServiceForUpload() {
        if (this.csvRecords[0].invalid.length) {
            this.snackBar.showErrorSnackBar(`CSV rejected; ${this.csvRecords[0].invalid.map(record => record.email)} invalid!`);
        } else {
            this.emailService.sendUploadedFileToServer(this.uploadedFile).subscribe(res => {
                this.studentEmails = res.students.created_emails;
                this.failedToCreateEmails = res.students.existing_emails;
                if (this.studentEmails.length) {
                    this.uploadCompleted = true;
                    this.snackBar.showSuccessSnackBar(`${this.studentEmails.length} Student account(s) created`);
                } else if (this.failedToCreateEmails.length) {
                    this.noEmails = true;
                    this.failedMaildataSource = this.failedToCreateEmails;
                    this.snackBar.showErrorSnackBar('Emails not created');
                }
            }, error => {
                this.snackBar.showErrorSnackBar(error.toString());
            });
        }
    }

    sendBulkEmail() {
        if (this.studentEmails.length) {
            this.emailService.bulkUploadEmail(this.studentEmails).subscribe(res => {
                this.snackBar.showSuccessSnackBar('Emails successfully sent');
            }, error => {
                this.snackBar.showErrorSnackBar(error.toString());
            });
        }
    }

    callServiceToExtractDataAndUpdateEmails = (validEmails: IEmail[]) => {
        this.dataSource = new EmailDataSource(this.paginator, validEmails, this.table);
        this.isLoaded = true;
    }

    getCohortsAndAssignToDropdown() {
        this.getCohortService.getCohorts().subscribe(res => {
            this.currentCohorts = res.cohorts;
        }, err => {
            this.snackBar.showErrorSnackBar(err.toString());
        });
    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
        this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    private _filter(value: string): ICohort[] {
        const filterValue = value.toLowerCase();
        const resultsFiltered = this.currentCohorts.filter(cohort => cohort.cohort_name.includes(filterValue));
        return resultsFiltered;
    }
}
