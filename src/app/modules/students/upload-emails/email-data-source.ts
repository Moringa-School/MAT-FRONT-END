import { DataSource } from "@angular/cdk/table";
import { IEmail } from "src/app/interfaces/email.interface";
import { MatPaginator } from "@angular/material/paginator";
import { MatTable } from "@angular/material/table";
import { Observable, of, merge } from "rxjs";
import { map } from "rxjs/operators";

export class EmailDataSource extends DataSource<IEmail> {
  data: IEmail[];
  constructor(
    private paginator: MatPaginator,
    private validEmails: IEmail[],
    private table: MatTable<any>
  ) {
    super();
    this.data = validEmails;
  }
  connect(): Observable<IEmail[]> {
    const dataMutations = [of(this.data), this.paginator.page];
    this.paginator.length = this.data.length;
    return merge(...dataMutations).pipe(
      map(() => {
        return this.getPagedData([...this.data]);
      })
    );
  }
  disconnect() { }
  private getPagedData(data: IEmail[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }
}
