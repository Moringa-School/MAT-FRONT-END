import { ComponentFixture, TestBed, async, tick, fakeAsync } from '@angular/core/testing';
import { UploadEmailsComponent } from './upload-emails.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmailService } from '../../../core/http/upload-emails/email.service';
import { GetCohortService } from '../../../core/http/get-cohorts/get-cohort.service';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatAutocompleteModule, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { ICohortResponse, ICohort } from 'src/app/interfaces/cohort.interface';
import { IEmailResponse } from 'src/app/interfaces/email.interface';
import { DebugElement } from '@angular/core';


describe('UploadEmailsComponent', () => {
  let component: UploadEmailsComponent;
  let fixture: ComponentFixture<UploadEmailsComponent>;
  const data = [{ Username: 'Henry', firstname: 'Onyango', email: 'henry.onyango@moringaschool.com', lastname: '1234566' }];
  let csvRecords: any[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      declarations: [UploadEmailsComponent],
      providers: [EmailService, GetCohortService]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadEmailsComponent);
    component = fixture.componentInstance;
  });

  it('should create instance of component', () => {
    expect(component).toBeTruthy();
  });

  it('should contain input with class name class_identifier', () => {
    const testInputField = fixture.nativeElement.querySelector('.class_identifier');
    expect(testInputField).toBeTruthy();
  });

  it('should contain input with class name file-upload', () => {
    const testInputField = fixture.nativeElement.querySelector('.file-upload');
    expect(testInputField).toBeTruthy();
  });

  it('should contain input with class name submitButton', () => {
    const testInputField = fixture.nativeElement.querySelector('.submitButton');
    expect(testInputField).toBeTruthy();
  });

  it('should call the method to send the file to the server', fakeAsync(() => {
    let element: DebugElement;
    const debugElement = fixture.debugElement;
    const emailService = debugElement.injector.get(EmailService);
    const response: IEmailResponse = {
      students: {
        created_count: 20,
        error_count: 1,
        existing_emails: ['ghjkh'],
        created_emails: ['dfghjkjh']
      }
    };
    component.csvRecords = [{
      invalid: [],
      valid: [{
        Class: "MC26",
        Username: "Cynthia",
        email: "wauuukasa@.com",
        first_name: "Cyn",
        second_name: "Kasa"
      }]
    }]
    element = fixture.debugElement.query(By.css('#input'))
    fixture.detectChanges()
    spyOn(component, 'uploadCSVFile');
    element.triggerEventHandler('change', {})
    component.uploadCSVFile('change')
    const spyOnUpload = spyOn(emailService, 'sendUploadedFileToServer').and.returnValue(of(response));
    component.sendFileToServiceForUpload();
    fixture.whenStable().then(() => {
      expect(spyOnUpload).toHaveBeenCalled();
      tick(2000);
    });
  }));

  it('should check that the pageSizeOption has been set', () => {
    component.setPageSizeOptions('setPageSize');
    expect(component.pageSizeOptions = 'setPageSize'.split(',').map(str => +str)).toBeTruthy();
  });

  it('should receive cohorts from the service and assign them to a variable', fakeAsync(() => {
    const result: ICohort = { cohort_name: 'mc33', id: 2 };
    const response: ICohortResponse = {
      cohorts: []
    };
    const debugElement = fixture.debugElement;
    const cohortService = debugElement.injector.get(GetCohortService);
    const spyService = spyOn(cohortService, 'getCohorts').and.returnValue(of(response));
    component.getCohortsAndAssignToDropdown();
    tick(2000);
    fixture.whenStable().then(() => {
      expect(spyService).toHaveBeenCalled();
    });
  }));

});
