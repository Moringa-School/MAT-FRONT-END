import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuardService } from "../../core/guards/auth/auth.guard";
import { CreateCohortComponent } from "./create-cohort/create-cohort.component";
import { ListStudentsComponent } from "./list-students/list-students.component";
import { StudentStatusEmailResendComponent } from "./student-status-email-resend/student-status-email-resend.component";
import { UploadEmailsComponent } from "./upload-emails/upload-emails.component";

const listStudentsRoutes: Routes = [
  {
    path: "create-cohort",
    canActivate: [AuthGuardService],
    component: CreateCohortComponent,
  },
  {
    path: "",
    component: ListStudentsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "resend-status-email",
    canActivate: [AuthGuardService],
    component: StudentStatusEmailResendComponent,
  },
  {
    path: "upload-email",
    canActivate: [AuthGuardService],
    component: UploadEmailsComponent,
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(listStudentsRoutes)],
  exports: [RouterModule],
})
export class StudentRoutingModule {}
