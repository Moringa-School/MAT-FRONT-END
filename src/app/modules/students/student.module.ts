import { NgModule } from "@angular/core";
import { MaterialModule } from "src/app/shared/material.module";
import { CreateCohortComponent } from "./create-cohort/create-cohort.component";
import { ListStudentsComponent } from "./list-students/list-students.component";
import { StudentRoutingModule } from "./student-routing.module";
import { UploadEmailsComponent } from "./upload-emails/upload-emails.component";
import { SharedModule } from "src/app/shared/shared.module";
import { AssignCohortComponent } from '../admin/assign-cohort/assign-cohort.component';
import { StudentStatusEmailsComponent } from './student-status-emails/student-status-emails.component';
import { StudentStatusEmailResendComponent } from './student-status-email-resend/student-status-email-resend.component';

@NgModule({
  imports: [MaterialModule, SharedModule, StudentRoutingModule],
  declarations: [
    CreateCohortComponent,
    ListStudentsComponent,
    UploadEmailsComponent,
    AssignCohortComponent,
    StudentStatusEmailsComponent,
    StudentStatusEmailResendComponent,
  ],
  exports: [
    CreateCohortComponent,
    ListStudentsComponent,
    UploadEmailsComponent,
    StudentStatusEmailResendComponent,
  ],
})
export class StudentModule {}

