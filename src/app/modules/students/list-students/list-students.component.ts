import { Component, ViewChild } from "@angular/core";
import { PageEvent, MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { Observable } from "rxjs";

import { IStudentsResponse } from "src/app/interfaces/data.interface";
import {ICohortResponse} from '../../../interfaces/cohort.interface';
import { ListStudentsService } from "src/app/core/http/list-students/list-students.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { StudentStatusEmailsComponent } from '../student-status-emails/student-status-emails.component';
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { GetCohortService } from '../../../core/http/get-cohorts/get-cohort.service';

@Component({
  selector: "app-list-students",
  templateUrl: "./list-students.component.html",
  styleUrls: ["./list-students.component.scss"],
})
export class ListStudentsComponent {
  students: MatTableDataSource<any>;
  displayedColumns = ["username", "name", "email", "created_at", "updated_at"];

  pageNumber: number = 1;
  pageSize: number = 5;
  totalStudents: number = 0;
  pageSizeOptions: Array<number> = [3, 5, 10, 15, 20, 50];

  pageEvent: PageEvent;
  cohorts = [];
  selectedcohort: any

  @ViewChild("studentsPaginator") paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private listStudentsService: ListStudentsService, 
    private getCohortService : GetCohortService,
    private dialog: MatDialog,
    private snackBar: SnackBarService,
    ) {}

  ngOnInit() {
    this.fetchStudentsByPage(this.pageNumber);
  }

  openSendEmailDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.height = '500px';
    dialogConfig.width = '500px';
    this.dialog.open(StudentStatusEmailsComponent, dialogConfig);
  }

  onPaginateChange = ($event,) => {
    if(this.selectedcohort){
      this.getStudentsInCohort(this.selectedcohort,$event.pageIndex + 1)
    }else{
      this.fetchStudentsByPage($event.pageIndex + 1);
    }
   
  };

  fetchStudentsByPage = (pageNumber) => {
    this.listStudentsService.getStudents(pageNumber).subscribe(
      (result: IStudentsResponse) => {
        this.students = new MatTableDataSource(result.students.results);
        setTimeout(() => (this.students.paginator = this.paginator));
        this.totalStudents = result.students.count;
        setTimeout(() => (this.students.sort = this.sort));
      },
      (error) => { 
        this.snackBar.showErrorSnackBar(error.statusText)
      }
      );
  };

  fetchCohorts = () => {
    this.getCohortService.getCohorts().subscribe(
      (result:ICohortResponse) => {
        this.cohorts = result.cohorts;      
      }, error => {
          this.snackBar.showErrorSnackBar(error.statusText);
      }
    );
  } 

  getStudentsInCohort=(cohortId,pageNumber) => {
    this.listStudentsService.getStudentsInCohort(cohortId=this.selectedcohort,pageNumber).subscribe(  
      (result: IStudentsResponse) => {
        this.students = new MatTableDataSource(result.students.results);
        setTimeout(() => (this.students.paginator = this.paginator));
        this.totalStudents = result.students.count;
        setTimeout(() => (this.students.sort = this.sort));
      }, error => {
          this.snackBar.showErrorSnackBar(error.statusText);
      }
    )
  }

}



