import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  tick,
  fakeAsync,
} from "@angular/core/testing";
import { of } from 'rxjs/internal/observable/of';
import { DebugElement } from '@angular/core';
import { ListStudentsComponent } from "./list-students.component";
import { MatTableModule } from "@angular/material/table";
import { MatDialogModule,MatSnackBarModule } from "@angular/material";
import { MatPaginatorModule, MatPaginator } from "@angular/material/paginator";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { ListStudentsService } from "../../../core/http/list-students/list-students.service";
import { By } from "@angular/platform-browser";
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import {GetCohortService} from '../../../core/http/get-cohorts/get-cohort.service'
import {ICohortResponse,ICohort} from '../../../interfaces/cohort.interface'

/* tslint:disable */
describe("ListStudentsComponent", () => {
  let component: ListStudentsComponent;
  let fixture: ComponentFixture<ListStudentsComponent>;
  let snackBarServiceTest: SnackBarService;
  let listStudentsService: ListStudentsService;
  let getCohortService: GetCohortService;
  const testStudents = {
    "students": {
      "count": 10,
      "next": 'string',
      "previous": 'string',
      "results": [
        {
          "first_name": 'string',
          "last_name": 'string',
          "email": 'dummy@mail.com',
          "username": 'dummy',
          "created_at": '01-01-2020',
          "updated_at": '01-01-2020'
        }
      ]
    }
  }
  const result: ICohort = { cohort_name: 'mc33', id: 2 };
  const response: ICohortResponse = {
    cohorts: []
  };
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        BrowserAnimationsModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatSnackBarModule,
      ],
      declarations: [ListStudentsComponent],
      providers: [ListStudentsService, HttpTestingController],
    });
    fixture = TestBed.createComponent(ListStudentsComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    snackBarServiceTest = TestBed.get(SnackBarService);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created', inject([ListStudentsService], (service: ListStudentsService) => {
    expect(service).toBeTruthy();
  }));

  it('should be created', inject([SnackBarService], (service: SnackBarService) => {
    expect(service).toBeTruthy();
  }));

  it('should be created', inject([GetCohortService], (service: GetCohortService) => {
    expect(service).toBeTruthy();
  }));

  it("should have a wrapper class student-list-container, spinner-container and students-table", fakeAsync(() => {
    const element = fixture.debugElement;
    expect(element.queryAll(By.css(".student-list-container")).length).toBe(1);
  }));

  it("shoud call the fetch students function",fakeAsync(()=>{
    let fetchStudentsButton =fixture.debugElement.query(By.css('.list-cohorts')); 
    const spy = spyOn(component, 'fetchCohorts');
    fetchStudentsButton.nativeElement.click()
    tick();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }))

  it('should call the fetch cohorts function and return a response', fakeAsync(() => {
    const debugElement = fixture.debugElement;
    const cohortService = debugElement.injector.get(GetCohortService);
    const spyService = spyOn(cohortService, 'getCohorts').and.returnValue(of(response));
    component.fetchCohorts();
    tick(2000);
    fixture.whenStable().then(() => {
      expect(spyService).toHaveBeenCalled();
    });
  }))

  it('should call the fetch all students function and return a response', fakeAsync(() => {
    const debugElement = fixture.debugElement;
    const studentCohortService = debugElement.injector.get(ListStudentsService);
    const spyService = spyOn(studentCohortService, 'getStudents').and.returnValue(of(testStudents));
    component.fetchStudentsByPage(1);
    tick(2000);
    fixture.whenStable().then(() => {
      expect(spyService).toHaveBeenCalled();
    });
  }))
  
});
