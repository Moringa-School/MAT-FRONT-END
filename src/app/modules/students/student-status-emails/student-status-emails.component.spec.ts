import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule, MatInputModule, MatSnackBarModule, MatDialogModule } from '@angular/material';


import { StudentStatusEmailsComponent } from './student-status-emails.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';



describe('StudentStatusEmailsComponent', () => {
  let component: StudentStatusEmailsComponent;
  let fixture: ComponentFixture<StudentStatusEmailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentStatusEmailsComponent],
      imports: [MatFormFieldModule, MatInputModule,
        BrowserModule /* or CommonModule */,
        FormsModule, ReactiveFormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentStatusEmailsComponent);
    component = fixture.componentInstance;
    component.display = true;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
