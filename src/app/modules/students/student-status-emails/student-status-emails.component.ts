import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GetCohortService } from 'src/app/core/http/get-cohorts/get-cohort.service';
import { StatusEMailService } from 'src/app/core/http/upload-final-list/upload-final-list.service';
import { SnackBarService } from 'src/app/core/http/snackbar/mat-snack-bar.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-student-status-emails',
  templateUrl: './student-status-emails.component.html',
  styleUrls: ['./student-status-emails.component.scss']
})
export class StudentStatusEmailsComponent implements OnInit {
  isLoading = false;
  cohorts = [];
  submitted = false;
  statusEmailForm: FormGroup;
  modules = [{ 'level': 'END OF MODULE', 'value': 'final' }, { 'level': 'MID MODULE', 'value': 'mid' }];
  progress
  fileName = "Upload FIle CSV... "
  fileError = false;
  extensionError = false;
  display: boolean = true

  constructor(
    private formBuilder: FormBuilder,
    private getCohortService: GetCohortService,
    private snackBarService: SnackBarService,
    private statusEMailService: StatusEMailService,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getCohortsList();
    this.statusEmailForm = this.formBuilder.group({
      file: ['', [Validators.required]],
      fileSource: ['', [Validators.required]],
      nextModule: ['', [Validators.required]],
      cohorts: [, [Validators.required]],
      level: ['', [Validators.required]]
    });
  }

  validateName(fileName) {
    if ((fileName.includes("MPFT")) || (fileName.includes("MPP")) || (fileName.includes("DSPFT")) || (fileName.includes("MC"))) {
      this.fileName = fileName
      this.fileError = false;
    } else {
      this.fileError = true;

    }
  }

  cancel(): void {
    this.dialog.closeAll();
  }

  validateFileExtension(fileName) {
    const extension = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (!['csv'].includes(extension)) {
      this.extensionError = true;
    } else {
      this.extensionError = false
    }
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.validateName(file.name);
      this.validateFileExtension(file.name);

      this.statusEmailForm.patchValue({
        fileSource: file
      });
    }
  }

  checkStatus() {
    this.submitted = true;
    const formData = new FormData();
    const status = this.statusEmailForm.get('level').value;
    formData.append('file', this.statusEmailForm.get('fileSource').value);
    formData.append('cohort_id', this.statusEmailForm.get('cohorts').value);
    formData.append('next_module', this.statusEmailForm.get('nextModule').value);
    
    if (this.statusEmailForm.valid) {
      switch (status) {
        case 'final':
          this.uploadFinalListEmails(formData);
          break;
        case 'mid':
          this.uploadMidModuleEmails(formData);
          break;
        default:
          this.uploadFinalListEmails(formData);
      }
    }
  }


  checkHttpEVent(event, message) {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        this.isLoading = true;
        this.progress = Math.round(event.loaded / event.total * 100);
        if (this.progress === 100) {
          this.spinner.show();
        }
        break;
      case HttpEventType.Response:
        this.isLoading = false;
        this.spinner.hide();
        setTimeout(() => {
          this.progress = 0;
        }, 1500);
        this.dialog.closeAll();
        if (event.body.success) {
          this.snackBarService.showSuccessSnackBar(message);
        } else {
          this.snackBarService.showErrorSnackBar(event.body.error);
        }
    }
  }



  uploadFinalListEmails(data) {
    this.statusEMailService.uploadFinalList(data).subscribe((event: HttpEvent<any>) => {
      const message = "Final List status emails have successfully been sent."
      this.checkHttpEVent(event, message);
    },
      error => {
        this.spinner.hide()
        if (error) {
          error.error.file_error && this.snackBarService.showErrorSnackBar(error.error.file_error);
        }
      });
  }


  uploadMidModuleEmails(data) {
    this.statusEMailService.uploadMidModuleList(data).subscribe((event: HttpEvent<any>) => {
      const message = "Mid Module status emails have successfully been sent."
      this.checkHttpEVent(event, message);
    },
      error => {
        if (error) {

          this.snackBarService.showErrorSnackBar(error.error.file_error);
        }
      });
  }

  getCohortsList() {
    this.getCohortService.getCohorts().subscribe(res => {
      this.cohorts = res.cohorts;
    }, err => {
    });
  }
}
