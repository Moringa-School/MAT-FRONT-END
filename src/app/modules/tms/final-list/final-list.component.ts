import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalAuthService } from 'src/app/core/http/authentication/auth/newauth.service';


interface finalList {
  cohortName: string
  uploadDate: string
}

@Component({
  selector: 'app-final-list',
  templateUrl: './final-list.component.html',
  styleUrls: ['./final-list.component.scss']
})


export class FinalListComponent implements OnInit {

  constructor(
    private authService: GlobalAuthService,
    private router: Router,
  ) { }

  finalListInfo:finalList[]  = [
    {
      cohortName: "MPFT30",
      uploadDate: "12th May, 2020"
    }, 
    {
      cohortName: "MPFT31",
      uploadDate: "30th September, 2020"
    },
    {
      cohortName: "MPFT33",
      uploadDate: "30th December, 2020"
    },
    {
      cohortName: "MPFT35",
      uploadDate: "1st January, 2021"
    }
  ]

  ngOnInit(): void {
  }

  routeToPage = (pageName) => this.router.navigate([pageName]);

  onLogoutUser = () => {
    this.authService.logout()
    this.routeToPage('auth/login');
  };

}
