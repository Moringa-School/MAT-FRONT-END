import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FinalListComponent } from './final-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


describe('FinalListComponent', () => {
  let component: FinalListComponent;
  let fixture: ComponentFixture<FinalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FinalListComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule, FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
