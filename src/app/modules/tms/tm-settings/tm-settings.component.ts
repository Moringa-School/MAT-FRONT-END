import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { Router } from '@angular/router';
import { GlobalAuthService } from '../../../core/http/authentication/auth/newauth.service';
import { AssignCohortComponent } from '../../admin/assign-cohort/assign-cohort.component';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-tm-settings',
  templateUrl: './tm-settings.component.html',
  styleUrls: ['./tm-settings.component.scss']
})
export class TmSettingsComponent implements OnInit {

  invitesForm: FormGroup
  isLoading: false

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private authService: GlobalAuthService,
    private router: Router

  ) { }

  sendInvites() {

  }

  openAssignCohortDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    this.dialog.open(AssignCohortComponent, dialogConfig);
  }
  routeToPage = (pageName) => this.router.navigate([pageName]);

  onLogoutUser = () => {
    this.authService.logout()
    this.routeToPage('auth/login');
  };

  ngOnInit(): void {
    this.invitesForm = this.formBuilder.group({
      emails: [' '],
    });
  }

}
