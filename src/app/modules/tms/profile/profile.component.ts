import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TmProfileService } from '../../../core/http/tms/profile.service'

interface cohortAllocation { 
  android: number 
  fullStack: number
  total: number
}
interface Cohort {  
     cohortName: string
     startDate?: string
     endDate?: string 
     cohortAllocation: cohortAllocation 
     attendancePercentage: number 
}
interface userInfo {
  technicalMentorName: string
  email: string
  role: string
}

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
  userInfo: userInfo;
  cohortHistory

  constructor
  (
    private tmProfileService: TmProfileService, 
  ) { }

  ngOnInit(): void {
    this.getProfileInformation();
  }

  getProfileInformation(){
    this.tmProfileService.getTmDetails().subscribe((res) => {
      const { tm } = res;
      const userInfo :  userInfo = {
        technicalMentorName: `${tm.first_name}${tm.last_name}`,
        email: tm.email,
        role: `Technical Mentor`
      }
      this.userInfo = userInfo;
      this.cohortHistory = tm.cohort_history 
    },
    (error) => {
      console.log(error)
    }
    )
  }
}
