import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuardService } from "../../core/guards/auth/auth.guard";
import { InviteTmsComponent } from "./invite-tms/invite-tms.component";
import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  {
    path: "invite-TM",
    canActivate: [AuthGuardService],
    component: InviteTmsComponent,
  },
  {
    path: "profile",
    // canActivate: [AuthGuardService],
    component: ProfileComponent, 
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TMRoutingModule {}
