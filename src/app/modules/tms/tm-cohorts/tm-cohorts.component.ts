import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { GlobalAuthService } from 'src/app/core/http/authentication/auth/newauth.service';
import { Router } from '@angular/router';



interface cohortAllocation {
  android: number
  fullStack: number
  total: number
}

interface Cohort {
  cohortName: string
  startDate?: string
  endDate?: string
  cohortAllocation: cohortAllocation
  attendancePercentage: number
}

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-tm-cohorts',
  templateUrl: './tm-cohorts.component.html',
  styleUrls: ['./tm-cohorts.component.scss']
})

export class TmCohortsComponent implements OnInit {

  modifiedHistoryData;

  cohorts: Cohort[] = [
    {
      cohortName: 'MC29',
      cohortAllocation: {
        android: 10,
        fullStack: 20,
        total: 30
      },
      attendancePercentage: 50,
      startDate: 'September 14th 2020',
      endDate: 'January 10th 2021'
    },
    {
      cohortName: 'MC30',
      cohortAllocation: {
        android: 10,
        fullStack: 20,
        total: 30
      },
      attendancePercentage: 50,
      startDate: 'September 14th 2020',
      endDate: 'January 10th 2021'
    },
    {
      cohortName: 'MC31',
      cohortAllocation: {
        android: 10,
        fullStack: 20,
        total: 30
      },
      attendancePercentage: 50,
      startDate: 'September 14th 2020',
      endDate: 'January 10th 2021'
    }

  ]
  @Input() cohortHistory

  constructor(
    private authService: GlobalAuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.modifyCohortHistoryData()
  }

  routeToPage = (pageName) => this.router.navigate([pageName]);

  onLogoutUser = () => {
    this.authService.logout()
    this.routeToPage('auth/login');
  };


  modifyCohortHistoryData() {
    console.log(this.cohortHistory, '.......');
    this.modifiedHistoryData = []
    this.cohortHistory.forEach((item) => {
      const cohortDetails = {
        cohortName: item.cohort_name,
        cohortAllocation: {
          android: 10,
          fullStack: 20,
          total: 30
        },
        attendancePercentage: Math.floor(Math.random() * 100),
        startDate: 'September 14th 2020',
        endDate: 'January 10th 2021'
      }
      this.modifiedHistoryData.push(cohortDetails);
      console.log(this.modifiedHistoryData)
    })
  }

}
