import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TmCohortsComponent } from './tm-cohorts.component';

describe('TmCohortsComponent', () => {
  let component: TmCohortsComponent;
  let fixture: ComponentFixture<TmCohortsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TmCohortsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmCohortsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
