import { NgModule } from '@angular/core';
import  { CommonModule } from '@angular/common';

import {InviteTmsComponent} from './invite-tms/invite-tms.component'
import { MaterialModule } from "src/app/shared/material.module";
import { TMRoutingModule } from './tms-routing.module';

@NgModule({
    declarations: [InviteTmsComponent],
    imports: [MaterialModule, CommonModule, TMRoutingModule],
  })

export class TMModule {}
