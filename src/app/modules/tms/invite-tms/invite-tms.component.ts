import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from "@angular/material";
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { ITMEmailInvite } from 'src/app/interfaces/email.interface';
import { InviteTmsService } from '../../../core/http/tms/invite-tms.service';
@Component({
  selector: 'app-invite-tms',
  templateUrl: './invite-tms.component.html',
  styleUrls: ['./invite-tms.component.scss']
})
export class InviteTmsComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: SnackBarService,
    public dialog: MatDialog,
    private inviteTmsService: InviteTmsService,
  ) { }

  inviteTMForm: FormGroup;
  isLoading: boolean = false;
  submitted: boolean = false;
  error: boolean = false;
  tms: Object = {};

  successMessage = 'TM created successfully!';

  ngOnInit(): void {
    this.inviteTMForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[^ @]*@[^ @]*')]],
    })
  }
  inviteTM = () => {
    if (this.inviteTMForm.valid) {
      this.submitted = true;
      this.isLoading = true;
      const tmInviteCredentials: ITMEmailInvite = {
        email: this.inviteTMForm.controls.email.value,
      }
      this.inviteTmsService.inviteTM(tmInviteCredentials).subscribe(
        res => {
          this.isLoading = false;
          this.error = false;
          this.snackBar.showSuccessSnackBar(this.successMessage);
          this.dialog.closeAll();
        }, error => {
          this.isLoading = false;
          this.error = true;
        }
      )
    }
  }

}
