import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InviteTmsComponent } from './invite-tms.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SnackBarService } from '../../../core/http/snackbar/mat-snack-bar.service';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatFormFieldModule, MatSnackBarModule, MatDialogModule, MatInputModule } from '@angular/material';
import { InviteTmsService } from 'src/app/core/http/tms/invite-tms.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('InviteTmsComponent', () => {
  let component: InviteTmsComponent;
  let fixture: ComponentFixture<InviteTmsComponent>;
  let service: InviteTmsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      declarations: [InviteTmsComponent],
      providers: [SnackBarService, InviteTmsService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteTmsComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
    service = TestBed.get(InviteTmsService);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
