import {
    async,
    ComponentFixture,
    fakeAsync,
    TestBed,
    tick,
    flush,
} from "@angular/core/testing";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";

import { MatDialogModule, MatSnackBarModule } from "@angular/material";
import { AssignCohortComponent } from './assign-cohort.component';
import { IStaff, IStaffResponse } from 'src/app/interfaces/staff.interface';
import { GetStaffService } from 'src/app/core/http/get-staff/get-staff.service';
import { of } from 'rxjs';
import { DebugElement } from '@angular/core';
import { AssignTMToCohortService } from 'src/app/core/http/assign-cohort/assign-cohort.service';
import { By } from '@angular/platform-browser';
import { GetCohortService } from 'src/app/core/http/get-cohorts/get-cohort.service';
import { ICohort, ICohortResponse } from 'src/app/interfaces/cohort.interface';

describe("AssignCohortComponent", () => {
    let component: AssignCohortComponent;
    let fixture: ComponentFixture<AssignCohortComponent>;

    const sampleDataTMs: IStaff =
    {
        "first_name": "First",
        "last_name": "Last",
        "email": "test_tm@mail.com",
        "username": "technical mentor"
    };

    const response: IStaffResponse = {
        staff: {
            results: [sampleDataTMs],
            count: 1,
            next: "one"
        }
    }

    const sampleCohorts: ICohort = {
        cohort_name: "MC30",
        id: 3
    }

    const cohortResponse: ICohortResponse = {
        cohorts: []
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatButtonModule,
                MatInputModule,
                RouterTestingModule,
                MatFormFieldModule,
                BrowserAnimationsModule,
                ReactiveFormsModule,
                FormsModule,
                HttpClientTestingModule,
                MatSnackBarModule,
                MatDialogModule
            ],
            declarations: [AssignCohortComponent],
            providers: [
                AssignTMToCohortService,
                GetCohortService
            ],
        });

        fixture = TestBed.createComponent(AssignCohortComponent);
        component = fixture.componentInstance;
        component.ngOnInit();
    }));

    it('should expect getAllTms method to be called', fakeAsync(() => {
        const debugElement = fixture.debugElement;
        const tmService = debugElement.injector.get(GetStaffService);
        const spyOnTmsService = spyOn(tmService, "getAllStaff").and.returnValue(of(response));
        component.getAllTMs();
        tick(1000);
        fixture.whenStable().then(() => {
            expect(spyOnTmsService).toHaveBeenCalled();
        });

    }))

    it('should expect getAllCohorts method to be called', fakeAsync(() => {
        const debugElement = fixture.debugElement;
        const cohortService = debugElement.injector.get(GetCohortService);
        const spyOnCohortService = spyOn(cohortService, "getCohorts").and.returnValue(of(cohortResponse));
        component.getAllCohorts();
        tick(1000);
        fixture.whenStable().then(() => {
            expect(spyOnCohortService).toHaveBeenCalled();
        });

    }))

    it("should expect the cohort and tm assigned", fakeAsync(() => {
        const response = {
            cohort: 'MC233',
            list_of_tms: ['MC233 TM1', 'MC233 TM2']
        }
        let assignElement: DebugElement;
        const debugElement = fixture.debugElement;
        let assignCohortService = debugElement.injector.get(AssignTMToCohortService);
        let assignSpy = spyOn(assignCohortService, "assignTmToCohort").and.returnValue(of(response));
        assignElement = fixture.debugElement.query(By.css('#form'));
        component.assignCohortsForm.controls['cohortControl'].setValue('MC233');
        component.assignCohortsForm.controls['listOfTms'].setValue(['MC233 TM1', 'MC233 TM2']);
        assignElement.triggerEventHandler('ngSubmit', null);
        fixture.whenStable().then(() => {
            expect(assignSpy).toHaveBeenCalled();
            tick()
        })
        component.dialog.closeAll();
        flush();
    }))
});
