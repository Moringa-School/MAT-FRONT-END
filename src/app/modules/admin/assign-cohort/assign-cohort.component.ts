import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AssignTMToCohortService } from 'src/app/core/http/assign-cohort/assign-cohort.service';
import { GetCohortService } from 'src/app/core/http/get-cohorts/get-cohort.service';
import { ICohort } from 'src/app/interfaces/cohort.interface';
import { IStaff } from 'src/app/interfaces/staff.interface';
import { GetStaffService } from 'src/app/core/http/get-staff/get-staff.service';
import { SnackBarService } from 'src/app/core/http/snackbar/mat-snack-bar.service';

@Component({
    selector: "app-assign-cohort",
    templateUrl: "./assign-cohort.component.html",
    styleUrls: ["./assign-cohort.component.scss"],
})
export class AssignCohortComponent implements OnInit {

    assignCohortsForm: FormGroup
    availableCohorts: ICohort[] = []
    allTMs: IStaff[] = []
    tmsSelected: FormControl = new FormControl()
    assignTMSuccessMessage = "TM(s) assigned successfully to cohort";
    assignTMFailureMessage = "Failed to assign TM(s) to Cohort";



    constructor(
        private formBuilder: FormBuilder,
        private assignToCohortService: AssignTMToCohortService,
        private getCohortsService: GetCohortService,
        private getAllStaff: GetStaffService,
        public snackBar: SnackBarService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        this.getAllCohorts();
        this.getAllTMs();
        this.assignCohortsForm = this.formBuilder.group({
            cohortControl: ['', [Validators.required]],
            listOfTms: ['', [Validators.required]]
        });
    }

    public errorHandling = (control: string, error: string) => {
        return this.assignCohortsForm.controls[control].hasError(error);
    }

    getAllCohorts() {
        this.getCohortsService.getCohorts().subscribe(
            (res) => {
                (this.availableCohorts = res.cohorts);
            },
            (err) => {
                this.snackBar.showErrorSnackBar(err.toString())
            }
        );
    }

    getAllTMs() {
        this.getAllStaff.getAllStaff().subscribe(
            (res) => {
                (this.allTMs = res.staff.results)
            },
            (err) => {
                this.snackBar.showErrorSnackBar(err.toString());
                });
            }

    assignTMs() {
        let cohort = this.assignCohortsForm.get('cohortControl').value;
        let list_of_tms = this.assignCohortsForm.get('listOfTms').value;
        this.assignToCohortService.assignTmToCohort({ cohort,list_of_tms }).subscribe(
            () => {
                this.snackBar.showSuccessSnackBar(this.assignTMSuccessMessage);
                this.dialog.closeAll();
            },
            () => {
                this.snackBar.showErrorSnackBar(this.assignTMFailureMessage);
            }
        )

    }
}
