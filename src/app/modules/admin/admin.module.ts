import { NgModule } from "@angular/core";
import { MaterialModule } from "src/app/shared/material.module";
import { SharedModule } from "src/app/shared/shared.module";
import { AssignCohortComponent } from '../admin/assign-cohort/assign-cohort.component';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  imports: [MaterialModule, SharedModule,AdminRoutingModule ],
  declarations:[
    AssignCohortComponent
  ],
  exports: [
  
  ],
})
export class AdminModule {}
