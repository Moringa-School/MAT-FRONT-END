import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

const adminRoutes = [
  
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
