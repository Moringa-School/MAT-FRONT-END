export interface generateOTP {
  success: boolean;
  message: string;
  hash: string;
  otp: number;
}

export interface verifyOTP {
  otp: number;
}

export interface verifyOTPResponse {
  success: boolean;
  message: string;
}
