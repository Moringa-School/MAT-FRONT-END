export interface IStudent {
     id :  number ,
     email :  string ,
     phone :  string ,
     name :  string ,
     website :  string ,
}

export interface IStudentsResponse {
     "students": {
         "count": number,
         "next": string,
         "previous": string,
         "results": [
             {
                 "first_name": string,
                 "last_name": string,
                 "email": string,
                 "username": string,
                 "created_at": string,
                 "updated_at": string
             }
          ]
     }
}


