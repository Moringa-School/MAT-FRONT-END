export interface IAuthBody {
    email: string;
    password: string;
}
export interface IAuthResponse {
    refresh: string;
    access: string;
    // role: Role;
}

export interface ISocialAuthBody {
    token: String;
}

export enum Role {
    TM = 'TM',
    POD_LEADER = 'POD_LEADER',
    STUDENT = 'STUDENT'
}
