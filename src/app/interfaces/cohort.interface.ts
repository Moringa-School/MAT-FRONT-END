export interface ICohortResponse {
    "cohorts": []
}
export interface ICohort {
    cohort_name: string,
    id: number
}
