

export interface IStaffResponse{
    staff: {
        results: IStaff[],
        count: number,
        next: string,
    }
}

export interface IStaff{
    email:string,
    username: string,
    first_name: string,
    last_name: string,
}