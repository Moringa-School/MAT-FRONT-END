export interface IEmail {
    Username: string,
    email: string,
    first_name: string,
    second_name: string,
    Class: string
}

export interface IEmailResponse {
    students: IStudentEmail
}

interface IStudentEmail {
    created_count: number,
    error_count: number,
    existing_emails: string[],
    created_emails: string[]
}

export interface ITMEmailInvite {
    email: string;
}

export interface IStatusEmail {
    first_name: string,
    last_name: string,
    email: string,
    class: string,
    track: string,
    recommendation: string,
    status: boolean
}