import { NgModule } from "@angular/core";
import { RouterModule, Routes, PreloadAllModules } from "@angular/router";

// routes
const appRoutes: Routes = [

  {
    path: "",
    redirectTo: "students",
    pathMatch: "full",
  },
  {
    path: "auth",
    loadChildren: () =>
      import("./modules/authentication/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "students",
    loadChildren: () =>
      import("./modules/students/student.module").then((m) => m.StudentModule),
  },
  {
    path: "tms",
    loadChildren: () =>
      import("./modules/tms/tms.module").then((m) => m.TMModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
