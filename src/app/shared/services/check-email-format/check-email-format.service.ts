import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckEmailFormatService {

  constructor() { }

  validateEmail(emails) {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    const validEmails = [];
    const invalidEmails = [];
    for (const email of emails) {
      emailRegex.test(email.email) ? validEmails.push(email) : invalidEmails.push(email);
    }
    return { valid: validEmails, invalid: invalidEmails };
  }

}
