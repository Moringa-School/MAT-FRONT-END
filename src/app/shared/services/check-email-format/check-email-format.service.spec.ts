import { TestBed } from '@angular/core/testing';

import { CheckEmailFormatService } from './check-email-format.service';

describe('CheckEmailFormatService', () => {
  let service: CheckEmailFormatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckEmailFormatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
