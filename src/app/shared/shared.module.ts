/**
    This module here registers all shared elements i.e. pipes and directives created in the services folder
    **/

import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TopNavComponent } from "./components/top-nav/top-nav.component";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "./material.module";



@NgModule({
  declarations: [],
  imports: [MaterialModule, FormsModule, ReactiveFormsModule, CommonModule],
  exports: [FormsModule, ReactiveFormsModule],
  providers: [],
})
export class SharedModule {}
