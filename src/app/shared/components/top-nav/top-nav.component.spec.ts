import { TestBed, async } from '@angular/core/testing';
import { TopNavComponent } from './top-nav.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';

/* tslint:disable */
describe('TopNavComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TopNavComponent
      ],
      imports: [
        RouterTestingModule,
        MatIconModule,
        MatToolbarModule,
        MatMenuModule,
        HttpClientTestingModule,
        MatDialogModule
      ]
    }).compileComponents();
  }));

  it('should have top-nav, right-nav, navbar-items hidden when isLoggedIn is false', () => {
    const fixture = TestBed.createComponent(TopNavComponent);
    let component = fixture.componentInstance;
    expect(component.isLoggedIn).toBeFalsy()
    expect(fixture.nativeElement.querySelector('.top-nav')).toBeNull();
    expect(fixture.nativeElement.querySelector('.right-nav')).toBeNull();
    expect(fixture.nativeElement.querySelector('.navbar-items')).toBeNull();
  });
});
