import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { GlobalAuthService } from "../../../core/http/authentication/auth/newauth.service";
import { CreateCohortComponent } from "src/app/modules/students/create-cohort/create-cohort.component";
import { InviteTmsComponent } from "../../../modules/tms/invite-tms/invite-tms.component";
import { Router } from "@angular/router";
import { AssignCohortComponent } from 'src/app/modules/admin/assign-cohort/assign-cohort.component';
import { IAuthResponse } from "src/app/interfaces/auth.interface";

@Component({
  selector: "app-top-nav",
  templateUrl: "./top-nav.component.html",
  styleUrls: ["./top-nav.component.scss"],
})
export class TopNavComponent implements OnInit {
  isLoggedIn: Observable<IAuthResponse>;
  applicationName = "Attendance tracker";

  constructor(
    private authService: GlobalAuthService,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.currentUserValue;
  }

  openCreateCohortDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    this.dialog.open(CreateCohortComponent, dialogConfig);
  }

  openAssignCohortDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    this.dialog.open(AssignCohortComponent, dialogConfig);
  }
  onLogoutUser = () => {
    this.authService.logout();
  };

  openInviteTMDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    this.dialog.open(InviteTmsComponent, dialogConfig);
  }

  redirectToProfile() {
    this.router.navigate(['tms/profile']);
  }
}
