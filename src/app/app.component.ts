import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { GlobalAuthService } from "./core/http/authentication/auth/newauth.service";
import { IAuthResponse } from "./interfaces/auth.interface";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  isLoggedIn: Observable<IAuthResponse>;

  constructor(private authService: GlobalAuthService) {}

  ngOnInit() {
    this.isLoggedIn =  this.authService.currentUserValue;
  }
}
