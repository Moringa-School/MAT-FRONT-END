export const environment = {
  production: true,
  BASE_URL: 'https://matapi.work/api/',
  GOOGLELOGINPROVIDER: '1085784746590-ejc8llnujrhrhtoin87odfddf9svtd49.apps.googleusercontent.com',
  SENTRY_DSN: 'https://2ae1a1ab73db4b779388623b7b610366@o359908.ingest.sentry.io/5206811'
};
