
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { GoogleLoginProvider } from "angularx-social-login";

export const environment = {
  production: false,
  BASE_URL: "https://matapi.work/api/",
  GOOGLELOGINPROVIDER:
    "1085784746590-ejc8llnujrhrhtoin87odfddf9svtd49.apps.googleusercontent.com",
  SENTRY_DSN:
    "https://2ae1a1ab73db4b779388623b7b610366@o359908.ingest.sentry.io/5206811",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
